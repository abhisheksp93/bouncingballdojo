package ball.model;

public interface Behaviour {
    void behave(BallImpl Ball);
}
