package ball.model;

import ball.Ball;

import java.awt.*;
import java.util.ArrayList;

public class BallImpl implements Ball {
    protected int x;
    protected int y;
    protected int radius;
    private Behaviour[] behaviours;

    protected BallImpl(int x, int y, int radius, Behaviour ... behaviours) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.behaviours = behaviours;
    }

    protected BallImpl(int x, int y) {
        this(x, y, DEFAULT_RADIUS);
    }

    // DO NOT CHANGE
    @Override
    public int radius() {
        return radius;
    }

    // DO NOT CHANGE
    @Override
    public Point center() {
        return new Point(x, y);
    }

    @Override
    public void update(){
        for(Behaviour behaviour : behaviours){
            behaviour.behave(this);
        }
    };
}
