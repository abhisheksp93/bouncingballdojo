package ball.model;

import ball.Ball;

public class BallFactory {

    public static Ball[] all() {
        return new Ball[]{
                bouncingBall(75, 50, Ball.DEFAULT_RADIUS, new Bouncing(Bouncing.DOWN)),
                elasticBall(250, 100, Ball.DEFAULT_RADIUS, new Elastic(Elastic.SHRINK)),
                elasticBouncingBall(350, 50, Ball.DEFAULT_RADIUS, new Bouncing(Bouncing.DOWN), new Elastic(Elastic.SHRINK))
        };
    }

    private static Ball elasticBouncingBall(int centerX, int centerY,int radius, Behaviour bouncing, Behaviour elastic) {
        return new BallImpl(centerX, centerY, radius, bouncing, elastic);
    }

    public static Ball bouncingBall(int centerX, int centerY, int radius, Behaviour bouncing) {
        return new BallImpl(centerX, centerY, radius, bouncing);
    }

    public static Ball elasticBall(int centerX, int centerY, int radius, Behaviour elastic) {
        return new BallImpl(centerX, centerY, radius, elastic);
    }
}
